# Pipeline CI/CD de l'application VaporMap

## Description

VaporMap est une application web permettant de référencer des points GPS dans une base de données.
Ces points peuvent être affichés sur une carte.  
L'application VaporMap dispose d'un pipeline CI/CD permettant d'automatiser les opérations d'intégration, de livraison et de déploiement de façon automatique.  

> Cette application est une maquette développée rapidement dans un but pédagogique par Tristan Le Toullec.  
> Le pipeline est un projet réalisé par Rossif FETCHEPING dans le cadre de son Mastère en Infrastructures Cloud et DevOps.

## Architecture de l'application

L'application "VaporMap" est constituée de 2 parties :  

* Le frontend, chargé de l'interface avec l'utilisateur dans le navigateur.
* L'API REST, chargée de :  
  * gérer les données  (création des points de relevé)
  * de fournir les informations sur les points au format "geojson" [https://fr.wikipedia.org/wiki/GeoJSON](https://fr.wikipedia.org/wiki/GeoJSON)

L'utilisateur final de l'application interragit avec le frontend.

__Le frontend__ est composé de fichiers statiques. Il execute du code javascript dans le navigateur de l'utilisateur, et effectue des requètes vers l'API. Il permet à l'utilisateur d'ajouter des points de relevés, et d'afficher les points saisis sur une carte.

__L'API__ est interrogée par le navigateur de l'utilisateur. Elle peut également être utilisée sans le frontend.
Elle est développée avec le framework Python Flask, un micro framework open-source de développement web en Python.  

Flask permet de définir des "routes", et d'y associer un traitement en python. Pour l'application Vapormap :  

* "/geojson" : export de la liste des points au format geojson  
* "/api/points" : api de gestion des points  

__La base de données__ est un SGBD MariaDB. Elle stocke les points référencés par l'utilisateur et est consultée par l'API qui stocke ou récupère les points.

## Créer les variables Gitlab du projet

Pour deployer VaporMap à partir du pipeline, il faut créer les variables suivantes sur Gitlab avec les valeurs de votre choix pour assurer le fonctionnement de la base de données : __DB_NAME__, __DB_USER__ et __DB_PASS__.  
Pour le faire, il faut se rendre sur l'interface Gitlab du projet à partir de l'onglet __Settings > CI/CD > Variables__.  

> Il est important de cocher l'option "Mask variable" pour assurer la sécurité des secrets de connexion à la BDD dans le pipeline.  

## Mettre en place l'environnement de production

Pour déployer VaporMap, il faut un environnement Docker + Docker Compose sur lequel on peut exécuter des commandes Shell. Il faut ensuite installer un runner dessus qui va communiquer avec Gitlab.  

### Installer runner sur une instance Linux

```sh
# Télécharger le binaire pour linux
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Donner la permission d'exécuter
sudo chmod +x /usr/local/bin/gitlab-runner

# Créer un utilisateur GitLab Runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Ajouter un utilisateur dans le groupe Docker
usermod -aG docker gitlab-runner

# Installer et exécuter en tant que service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

```

### Enregistrer un runner pour le projet

```sh
sudo gitlab-runner register \
  --non-interactive \
  --url "GITLAB_PROJECT_URL" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "shell" \
  --description "shell-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "vdi-icd"
```

## Lancer le pipeline

Pour déclancer le pipeline, il faut effectuer un commit sur le projet avec la phrase suivante dans le message du commit :  __run-all and deploy-app__
