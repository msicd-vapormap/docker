#!/bin/sh
export VAPORMAP_FRONTEND_ROOT="/usr/share/nginx/html"
export VAPORMAP_URL_PORT="80"
envsubst '${VAPORMAP_URL_SERVERNAME},${VAPORMAP_URL_PORT},${VAPORMAP_FRONTEND_ROOT}' < nginx.conf.template > /etc/nginx/conf.d/default.conf
envsubst '${VAPORMAP_BACKEND}, ${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json
exec "$@"