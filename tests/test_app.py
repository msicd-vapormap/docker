from tests.conftest import client

def test_status_code_api(client):
	response = client.get('/api/points/?format=json')
	assert response.status_code == 200

def test_return_api_points(client):
	response = client.get('/api/points/?format=json')
	data = response.data.decode() #Permet de décoder la data dans la requête
	assert data == '[]\n'

def test_status_code_geojson(client):
	response = client.get('/geojson')
	assert response.status_code == 200

def test_return_api_geojson(client):
	response = client.get('/geojson')
	data = response.data.decode() #Permet de décoder la data dans la requête
	assert data == '{"features":[],"type":"FeatureCollection"}\n'