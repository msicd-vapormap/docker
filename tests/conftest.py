import pytest
from vapormap.app.api.app import app as flask_app
from vapormap.app.api.app import db

@pytest.fixture
def client():
    app = flask_app
    client = app.test_client()
    with app.app_context():
        db.create_all()
        yield client
        db.drop_all()