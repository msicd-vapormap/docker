# Projet fil rouge : Pipeline CI/CD avec Gitlab-CI

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet Fil Rouge  
__URL Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/r22fetch/vapormap-cicd>  
__URL Des Pages Gitlab__ : <https://r22fetch.gitlab-pages.imt-atlantique.fr/vapormap-cicd/>  
__Date__ : 13 Janvier 2023  

## Contexte

---

Ce projet consiste à mettre en place une chaine d'intégration continue et de déploiement continu de l'application VaporMap. Il s'agit d'une application web 3-tiers (Frontend, API et BDD) qui permet d'ajouter des points de relevés GPS et de les afficher sur une carte. Comme prérequis, je dispose d'une version "dockerisée" de l'application Vapormap constituée des fichiers (Dockerfile + dépendances) nécessaires pour la construction des images Docker de l'application et d'un fichier docker-compose pour instancier et orchestrer les containers à partir de ces images.  

Afin de réaliser ce projet, il sera question pour moi de:  

* Tester les composants de l'application Vapormap et automatiser ces tâches de tests  
* Automatiser la publication des résultats des tests pour qu'ils soient disponibles aux développeurs  
* Automatiser la construction des images Docker de Vapormap suivant des contraintes de qualité et de fiabilité de l'application et publier ces images dans la registry du projet  
* Automatiser le déploiement de l'application dans un environnement Docker

## Étape 1 : Intégration - Tests de l'application et publication des résultats

---

Dans cette étape d'intégration de Vapormap, j'ai réalisé en local des tests sur l'application qui m'ont produit des rapports. Par la suite, j'ai intégré ces opérations dans un pipeline à travers l'outil CI/CD de Gitlab qui m'a permis d'automatiser le traitement de ces tests suivant un canevas bien défini. De plus, j'ai ajouté dans ce pipeline une opération permettant de publier les rapports des tests sur un site web accessible aux développeurs de l'application.  

Ainsi, j'ai mis en place un pipeline qui se déclenche automatiquement et exécute suivant certaines contraintes les tests à chaque mise à jour du code et publie les rapports produits sur un site web accessible aux développeurs.

### Tests effectués

---

Afin d'améliorer l'application, j'ai mis en place des tests de qualité du code, des tests de fonctionnalités et des tests de couverture de code et j'ai sauvegardé les résultats de ces tests dans des fichiers.  
Pour ce faire, j'ai exploité des outils spécialisés choisis en fonction des critères suivants:  

* Open source  
* Documentation de l'outil  
* Facilité d'intégration  
* Rendu des résultats (qualitatif et quantitatif)  
* Popularité et cycle de vie de l'outil  

#### Tests de qualité de code

---

Afin de tester la qualité de code de Vapormap, je me suis servi des outils d'analyse de code (linter) qui permettent de détecter les erreurs et les problèmes de syntaxe. Pour fournir les meilleurs résultats, j'ai exploité un linter spécifique chaque langage de programmation utilisé dans l'application.  

__Qualité de code de l'API__  
L'API de Vapormap est codé avec Flask, un microframework basé essentiellement sur le langage Python.  
J'ai utilisé l'outil __Flake8__ pour évaluer la qualité du code Python.  
C'est un outil open source basé sur trois autres outils, il fournit ainsi des résultats très enrichis.  
Il est simple à mettre en oeuvre et dispose d'une documentation facile à prendre en main. L'outil est très populaire chez les développeurs et est régulièrement maintenu.  

__Qualité de code du Frontend__  
Le Frontend de Vapormap s'appuis sur la collection Bootstrap pour le rendu visuel HTML et CSS et sur du code Javascript pour consommer l'API. J'ai alors testé le code de tous ces langages.  

* J'ai utilisé __Bootlint__, un outil pour évaluer la qualité du code HTML (basé sur Bootstrap). C'est le seul outil  spécialisé en la matière, mais il remplit tous les critères de choix définis.  
* J'ai utilisé __JSHint__, un outil pour évaluer la qualité du code Javascript. Il qui répond mieux à nos critères de choix même s'il est moins populaire que ESlint. Il est plus facile à prendre en main et s'intègre parfaitement à notre application, car il permet d'analyser simplement le code Javascript inclus dans le fichier HTML du Frontend.
* J'ai utilisé __Stylelint__, un outil open source d'analyse du CSS. Le frontend contient également un bout de code CSS et je considère que ce dernier puisse évoluer. L'outil est efficace, très documenté et constamment maintenu par sa communauté. Il permet d'analyser le code CSS contenu dans un fichier HTML.

__Qualité de code des fichiers de Dockerisation__  
En plus de l'application proprement dite, Vapormap est également constitué du code des fichiers nécessaires à sa Dockerisation. On distingue notamment les Dockerfiles, les scripts shell et le docker compose. Dans le but d'assurer la qualité de ce code, j'ai effectué une analyse dessus grâce à des outils spécialisés remplissant mes critères de choix.  

* J'ai utilisé __Hadolint__ pour analyser le code des Dockerfiles. C'est un outil très populaire et efficace pour évaluer la qualité du code et proposer les améliorations à faire sur un Dockerfile. Il a répondu à tous nos critères de choix et s'est parfaitement intégré à notre projet.  
* J'ai utilisé __Shellcheck__ pour analyser le code Shell qui entre dans la construction de nos images Docker. C'est un outil open source qui trouve automatiquement les bugs dans les scripts shell.  
* J'ai utilisé __Yamllint__ pour analyser la qualité du code du fichier docker-compose.yml de l'application. Un outil open source, performant et très populaire pour vérifier la syntaxe et les bonnes pratiques sur un code YAML.
* J'ai également utilisé __Docker-compose__ avec l'option __config__ pour valider le code de la composition. Une fonctionnalité native de notre outil d'orchestration Docker Compose.

#### Tests fonctionnels

---

J'ai mis en place un scénario de test qui fournit le comportement l'API de Vapormap quand on veut récupérer la liste des points stockés en base de données. Cette méthode permet ainsi de valider le fonctionnement de l'application sans avoir besoin de la déployer.  
Pour le faire, j'ai utilisé l'outil __Pytest-Flask__, un module Python spécialisé pour tester le fonctionnement d'une application Flask. Il est très populaire chez les développeurs et répond à tous mes critères de choix.  

#### Tests de couverture de code

---

Dans l'optique d'optimiser le code de l'application, j'ai aussi effectué un test de couverture de l'API. Ce test permet de décrire le taux de code source exécuté quand la suite de tests fonctionnels est lancée.  
Pour le faire, j'ai utilisé l'outil __Pytest-cov__, un module Python complémentaire de Pytest pour évaluer la couverture de code.

### Publication des résultats

---

Après l'exécution de chaque test, j'ai formaté puis sauvegardé les résultats dans des fichiers markdown.  
J'ai ensuite utilisé l'outil __Material for MkDocs__ pour générer un site web statique à partir de ces fichiers pour permettre aux développeurs de les consulter.  
J'ai intégré ces opérations dans le pipeline sur Gitlab et j'ai exploité l'option __Gitlab Pages__ pour rendre le site accessible en ligne.  
__Lien pages :__ <https://r22fetch.gitlab-pages.imt-atlantique.fr/vapormap-cicd/>  

### Organisation du workflow

---

![Workflow Integration](<https://r22fetch.gitlab-pages.imt-atlantique.fr/vapormap-cicd/assets/workflow-integration.png>)  

> Pour cette phase d'intégration, les opérations de tests et de publication des résultats suscités ont été déclinées en __Jobs__ et organisées en __Stages__ dans l'outil pipeline de Gitlab CI/CD. Nous verrons dans cette partie le Workflow mis en place afin d'optimiser l'automatisation du processus d'intégration de l'application Vapormap.  

#### Les Stages

---

J'ai organisé les opérations en Stages qui vont s'executer de façon séquentielle (ou presque). Je distingue ainsi de 3 Stages à savoir:  

* __Codequality__ : il contient toutes les opérations visant à analyser la qualité du code  
* __Test__ : il contient toutes les opérations visant à tester le fonctionnement et la couverture du code une fois que le Codequality est passé avec succès  
* __Report__ : il contient toutes les opérations visant à publier les résultats générés par Codequality et Test  

> Les opérations contenues dans les __Stages__ sont elles-même organisées en __Jobs__ pour pouvoir être intégrées dans le pipeline de l'outil Gitlab. Nous verrons par la suite comment j'ai procédé. 

#### Les Jobs

---

Les fichiers de codes des composants (Frontend, API, Docker) de Vapormap étant distincts, j'ai créé des __Jobs__ indépendants pour tester chaque composant. Ainsi, le pipeline pourra déclencher des __Jobs__ concernant uniquement le(s) composant(s) modifié(s) à chaque mise à jour. Cette approche permet également d'optimiser le temps et les ressources pour le traitement des __Jobs__.  
J'ai alors organisé les __Jobs__ comme suite :  

* Dans le Stage __Codequality__, j'ai créé :  
  * Un Job __api_lint_job__ pour les opérations visant à analyser l'API (le code Python de l'API avec Flake8)  
  * Un Job __front_lint_job__ pour les opérations visant à analyser le frontend (le code HTML avec Bootlint, le code JS avec JSHint et le code CSS avec Stylelint)  
  * Un Job __api_docker_lint_job__ pour les opérations visant à analyser le Dockerfile de l'API avec Hadolint et son script dédié avec Shellcheck  
  * Un Job __front_docker_lint_job__ pour les opérations visant à analyser le Dockerfile du Frontend avec Hadolint et son script dédié avec Shellcheck  
  * Un Job __.docker_lint__ pour factoriser le code présent dans les deux Jobs précédents
  * Un Job __compose_lint_job__ pour les opérations visant à analyser et valider la composition avec YAMLLint et Docker Compose  
  * __Tous ces Jobs s'exécutent en parallèle et sauvegardent les résultats des tests dans des fichiers Markdown__  

* Dans le Stage __Test__, j'ai créé :  
  * Un Job __api_e2e_job__ pour les opérations visant à tester le fonctionnement de l'API avec Pytest-Flask  
  * Un Job __api_cov_job__ pour les opérations visant à tester la couverture de code de l'API avec Pytest-Cov  
  * Un Job __.test_api__ de factorisation de code présent dans les deux Jobs précédents  
  * __Tous ces Jobs s'exécutent en parallèle et sauvegardent les resultats des tests dans des fichiers Markdown__

* Dans le Stage __Report__, j'ai créé :  
  * Un Job __pages__ pour les opérations visant générer le site avec MkDocs et le publier sur Gitlab Pages.  
  * De plus, j'ai mis en place un système de cache pour conserver les derniers résultats de tests en mémoire au cas où un test n'est pas effectué parce que le Job de son composant n'est pas ajouté au démarrage du pipeline.  
  * __Ce Job exploite les fichiers sauvegardés dans les Jobs précédents grâce aux artifacts et/ou le cache pour générer le site__  

#### Les conditions

---

Afin d'optimiser le pipeline d'intégration de Vapormap, j'ai mis en place des conditions sur l'exécution des Jobs.  

* Les Jobs de __Codequality__ s'exécutent en même temps ou plus précisément en parallèle
* Les Jobs de __Test__ s'exécutent lorsque le Job de Codequality de l'API passe avec succès indépendamment de l'état des autres Jobs. J'ai  adopté cette condition parce que les Jobs de Test ne concernent que l'API qui doit être indépendante des autres composants.
* Le Job de __Report__ s'exécute toujours après tous les Jobs de Codequality et de Test sans tenir compte s'il y a eu des échecs ou pas afin de fournir les résultats des tests effectués.  
* Lors de l'exécution des Jobs de Codequality, j'ai défini des critères de blocage en spécifiant des seuils d'erreurs de sorte que si le nombre d'erreurs à la suite d'un test dépasse la valeur du seuil alors le Job est mis en échec. J'ai utilisé la commande __grep__ avec __les expressions régulières__ pour quantifier les erreurs fournies à la suite des tests.  
* Suite à une mise à jour du code, les Jobs concernant un composant de l'application (Frontend, API, Docker) sont ajoutés au pipeline s'il y'a eu des modifications sur les fichiers concernant le composant. Par exemple, le Job des tests sur le Frontend est pris en compte si et seulement s'il y'a eu des modifications dans le répertoire __frontend__ de Vapormap  
* On peut aussi choisir d'ajouter tous les __Jobs__ au pipeline sans tenir compte du composant modifié. Il suffit d'inclure dans le message du commit l'expression __run-all__.  
* J'ai également utilisé le runner de tag __ms-icd__ pour tous mes Jobs afin d'éviter les contraintes liées au Docker Hub avec les autres runners.  

> Les seuils de qualité ont été fixés de façon à permettre aux Jobs de passer avec succès :  
>
> * La couverture de code a un seuil de 50%.  
> * Le seuil d'erreurs pour les linters à 50 erreurs.  
> * Les Dockerfiles, leurs scripts et le docker compose ont un seuil de 1 erreur.  

## Étape 2 : Delivery

---

### Construction des images Docker et publication dans la registry Gitlab

---

Disposant des Dockerfiles, des scripts et de la composition, je me suis rassuré qu'aucune variable de déploiement de Vapormap n'est incluse dans les Dockerfiles et/ou les scripts afin d'assurer la personnalisation de l'application au déploiement. Toutes les variables sont alors définies dans la composition Docker Compose.  

* J'ai préalablement réalisé en local la construction des images Docker de Vapormap que j'ai ensuite orchestré avec Docker Compose afin de valider le fonctionnement de la stack.  
* J'ai ensuite automatisé l'opération de construction des images de l'API et du Frontend de Vapormap dans le pipeline Gitlab CI en exploitant un runner permettant de faire du DinD pour que notre container d'exécution Docker se connecte à la socket Docker du runner pour lancer les commandes nécessaires pour construire les images. Le runner tagué __ms-icd__ a été exploité pour cette opération.  
* J'ai nommé et tagué les images pendant l'opération de build afin qu'elles soient conformes pour la publication dans la registry et l'exploitation au déploiement.  
* J'ai assuré la connexion à la registry Gitlab du projet depuis le container d'exécution de l'opération et j'ai publié les images une fois leurs constructions terminées.  
* De plus, pour sécuriser le déploiement de l'application, j'ai copié l'image de MariaDB dans la registry du projet à partir du Docker Hub. J'ai exploité l'outil __Skopeo__ pour le faire et j'ai intégré cette tâche dans les opérations de mon pipeline.  
* De plus, j'ai conditionné l'exécution des tâches ci-dessus en fonction des résultats des tests précédemment implémentés sur les composants de l'application.

### Organisation du workflow

---

![Workflow Delivery](<https://r22fetch.gitlab-pages.imt-atlantique.fr/vapormap-cicd/assets/workflow-delivery.png>)  

#### Le Stage

---

Le Stage __Build__ basé sur deux Jobs a été créé pour les opérations de cette phase de delivery et intégré au pipeline précédemment mis en place. Je l'ai positionné à la suite du Stage de __Report__  

#### Les Jobs

---

Dans la même optique de rendre les composants de l'application indépendants les uns des autres, j'ai organisé les __Jobs__ du Stage __Build__ comme suite :  

* Un Job __api_build_job__ pour les opérations visant à _builder_ l'image du Frontend  
* Un Job __front_build_job__ pour les opérations visant à _builder_ l'image de l'API  
* Un Job __.build__ de factorisation de code présent dans les deux jobs précédents  
* L'opération de copie de l'image de MariaDB dans la registry Gitlab du projet est faite dans les deux Jobs de build mais elle s'exécute si et seulement si l'image n'existe pas dans la registry. J'ai mis en place cette vérification afin de ne pas répéter ce processus chaque fois que les Jobs sont exécutés.  

#### Les conditions

---

Afin d'optimiser le pipeline de delivery de Vapormap, j'ai mis en place des conditions suivantes sur l'exécution des Jobs :  

* Le Job de _build_ du frontend est ajouté au pipeline s'il y'a eu modification du code du frontend et/ou du Dockerfile/Script Shell de ce dernier  
* Afin d'assurer la qualité de l'image, l'exécution du Job de _build_ du frontend est conditionnée par le succès des Jobs de Codequality du frontend (code source et Dockerfile/Script Shell)
* Le Job de _build_ de l'API est ajouté au pipeline s'il y'a eu modification du code de l'API et/ou du Dockerfile/Script Shell de ce dernier  
* Afin d'assurer la qualité de l'image, l'exécution du Job de _build_ de l'API est conditionnée par le succès des Jobs de Test de l'API (Tests fonctionnels et Couverture de code). Précisons ici que les Jobs de test sont conditionnés en amont par le succès du Job de Codequality de l'API.  
* On peut aussi choisir d'ajouter tous les __Jobs__ de _build_ au pipeline sans tenir compte des composants modifiés. Il suffit d'inclure dans le message du commit l'expression __build-all__ ou plus généralement __run-all__ pour tous les Jobs du pipeline.  

## Étape 3 : Deploy

---

### Déploiement de Vapormap sur ma VM VDI

---

Afin de mettre en production Vapormap, j'ai intégré dans mon pipeline une étape déploiement de l'application sous forme de container Docker à partir des images "buildés". Le déploiement a été effectué sur l'environnement que j'ai mis en place sur ma VM VDI en exploitant les fonctionnalités offertes par les runners de Gitlab CI. Je vous propose ci-dessous les opérations que j'ai effectuées pour déployer automatiquement l'application.

Pour déployer l'application, il me fallait un environnement Docker sur lequel je pourrai exécuter des commandes Shell afin de lancer ma stack avec Docker Compose. Ayant à ma disposition une VM avec Docker/Docker-compose et Shell installés, il me suffisait d'installer un runner Gitlab à connecter à mon projet pour pouvoir déployer mon application dessus.

* Déploiement du runner sur la VM : en suivant la documentation de Gitlab,
  * J'ai téléchargé le binaire de gitlab-runner sur ma VM  
  * J'ai accordé les droits d'exécution à ce binaire  
  * J'ai créé un utilisateur gitlab-runner sur la VM  
  * J'ai installé et lancé le service à partir de cet utilisateur  
  * J'ai enregistré un runner avec un exécuteur Shell pour mon projet en me servant de l'URL et du Token fournis par Gitlab CI  
  * J'ai ajouté l'utilisateur gitlab-runner créé ci-dessus au groupe Docker de la VM afin qu'il puisse lancer les commandes Docker  

* Déploiement automatique de l'application sur la VM
  * J'ai créé un Job __deploy__ qui utilise la VM comme environnement d'exécution grâce au tag du runner
  * J'ai créé des variables sur Gitlab pour conserver les secrets de connexion à la base de données dans le docker-compose.  
  * J'ai exécuté dans ce Job une commande qui permet de se connecter à la registry du projet  
  * J'ai lancé la stack avec l'option détachée dans ce Job à partir du docker-compose afin qu'une fois déployée sur la VM, le Job s'achève.  

### Organisation du workflow

---

![Workflow Deploy](<https://r22fetch.gitlab-pages.imt-atlantique.fr/vapormap-cicd/assets/workflow-deploy.png>)  

* J'ai créé un Stage __Deploy__ intégré au pipeline précédemment mis en place. Je l'ai positionné à la suite du Stage de __Build__  
* Dans ce stage de __Deploy__, j'ai créé le Job __deploy__ pour les opérations visant à déployer l'application Vapormap sur ma VDI.  

#### Les conditions

---

* Après une mise à jour de l'application, le Job de _deploy_ est ajouté au pipeline si l'expression __deploy-app__ est incluse dans le message du commit.
* Le Job de _deploy_ s'exécute si les Jobs de _build_ et le Job de _Codequality_ de la composition Docker Compose nécessaire au déploiement sont passés avec succès.  

## Problèmes rencontrés et retour d'expérience

---

### Problèmes rencontrés

---

J'ai fait face à de nombreuses difficultés tout au long de ce projet et la liste est non exhaustive, mais je peux  notamment citer :  

* La mise en place des tests : Le travail avec les outils n'a pas été simple au début. Il fallait trouver le bon module avec la bonne configuration pour pouvoir l'exploiter. En plus, les Jobs etait systématiquement mis en échec une fois que les linters détectaient des erreurs.  
* Exploitation des résultats de tests : Les outils de tests ne disposent pas d'options pour pouvoir obtenir directement la quantité d'erreurs ou encore le taux de couverture. Après mes recherches, j'ai vu qu'il n'existe quasiment pas de solutions en ligne pour démêler ce problème.  
* Connaissance de Gitlab CI/CD : J'ai rencontré des difficultés dues à la connaissance de l'outil. Je me suis posé des questions face à certaines situations alors que la solution se retrouvait dans la documentation de Gitlab. Par exemple que l'on pourrait mettre __optional : true__ sur un __needs__ afin de ne pas avoir d'erreur si le job sollicité est ignoré par les conditions.  
* Le déploiement : Au départ, j'ai utilisé un runner en container docker sur ma VM pour déployer sur la machine elle même et j'ai tourné en rond avec ce choix. J'ai fini faire du SSH pour déployer avec cette approche mais les actions à réaliser pour arriver au résultat rend les choses beaucoup plus complexes pour la reproductibilité du pipeline.  

### Retour d'expérience

---

Ce projet a été très stimulant et riche en apprentissages. Il m'a permis de monter en compétences sur de nombreux sujets, notamment:  

* La mise en place des pipelines avec Gitlab CI  
* L'utilisation des runners Gitlab  
* L'utilisation de Mkdocs pour générer des sites avec des fichiers markdown  
* L'exploitation des variables pour gérer les secrets sur Gitlab  
* L'utilisation de la registry privé sur Gitlab  
* La connaissance des options de Gitlab CI/CD à travers large documentation et la communauté très active tout autour  

J'ai aussi renforcé mes acquis sur les sujets tels que :  

* La mise en place des tests d'application  
* La conteneurisation des applications avec Docker et l'orchestration Docker Compose  
* Le Scripting et plus précisemment l'utilisation de __grep__ et des expressions régulières  

Par ailleurs, je me suis posé beaucoup de questions et j'ai exploré énormément de choses pour acquérir le maximum de connaissances sur l'outil CI/CD de Gitlab à travers ce projet. J'ai ainsi pu me rendre compte que tout ceux à quoi j'ai pensé ou presque avait des solutions et pour la plupart dans la documentation officielle. C'est un outil assez complet et je compte approfondir davantage pour tirer le meilleur dans mes projets.  

## Annexes

---

### Installation d'un runner sur linux

```sh
# Télécharger le binaire pour linux
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Donner la permission d'exécuter
sudo chmod +x /usr/local/bin/gitlab-runner

# Créer un utilisateur GitLab Runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Ajouter un utilisateur dans le groupe Docker
usermod -aG docker gitlab-runner

# Installer et exécuter en tant que service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

### Enregistrer un runner pour le projet

```sh
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.imt-atlantique.fr/ " \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "shell" \
  --description "shell-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "vdi-icd"
```
