#!/bin/sh
cd api || exit
# wait for mariadb
counter=10
while [ $counter -gt 0 ] && ! mysql $VAPOR_DBNAME -h"$VAPOR_DBHOST" -u"$VAPOR_DBUSER" -p"$VAPOR_DBPASS" -e "SELECT 1;" > /dev/null 2>&1 ; do
    counter=$(( $counter - 1 ))
    echo "Waiting for MariaBD to be available"
    sleep 5
done
if [ $counter -le 0 ]; then
    echo >&2 'error: unable to contact MariaBD after 10 attempts at 5 second intervals'
    exit 1
fi
# end wait for mariadb
flask db upgrade
exec "$@"
